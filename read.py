import socket
from PIL import Image
import threading
import time

HOST = 'pixelflut' # CHANGE HERE!
PORT = 1234

def hex_to_rgb(value):
    value = value.lstrip('#')
    lv = len(value)
    return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))
                
                
class RecvThread(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.sock = sock
        self.bytesBuff = b''
        self.empty = False

    def run(self):
        while not self.empty:
            msg = self.sock.recv(2**32)
            if msg != b'':
                self.bytesBuff += msg
            else:
                if not self.empty:
                    print("empty")
                    self.empty = True
                    
class HandleThread(threading.Thread):
    def __init__(self, msg, count):
        threading.Thread.__init__(self)
        self.msg = msg 
        self.count = count
        self.broken = False

    def run(self):
        img = Image.new( 'RGB', (1920,1080), "black") # create a new black image
        pixels = img.load() # create the pixel map
        log = 0
        if self.msg != None:
            for px in self.msg.split("\n"):
                pixel = px.split(" ")
                if len(pixel) > 3:
                    try:
                        color = hex_to_rgb(pixel[3])
                        pixels[int(pixel[1]), int(pixel[2])] = (color[0], color[1], color[2])
                    except Exception as e:
                        print("exception with" + str(pixel))
                        print(e)
                        self.broken = True
                else:
                    self.broken = True
                    print("Broken Package! " + str(pixel))
        if not self.broken:
            print("BROKEN!")
        img.save("wz2/pixel"+ str(self.count) +".png") # CHANGE HERE!
        print("fertig")


maxx = 1920
maxy = 1080


with open("dump", 'w') as out:
    for x in range(0, maxx):
        for y in range(0, maxy):
            out.write("PX " + str(x) + " " + str(y)  + '\n')
print("generiert")


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((HOST, PORT))
send = sock.send
with open("dump", "r") as f:
    instruciton = f.read().encode()

def get_one_image(count):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((HOST, PORT))
    send = sock.send
    rt = RecvThread(sock)
    rt.start()
    print("sende")
    send(instruciton)
    empty = False
    print("empfange")
    rt.join()
    sock.close()
    print("processing")
    HandleThread(rt.bytesBuff.decode(), count).start()
i = 0
while 1:
    print(i)
    get_one_image(i)
    time.sleep(3)
    i += 1







